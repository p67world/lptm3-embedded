#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"

#include "hal.h"

void hal_vInit(void)
{
ret_code_t err_code;

    nrf_gpio_cfg_output(PORT_CTRL_SH);
    nrf_gpio_cfg_output(PORT_CTRL_FO);
    nrf_gpio_cfg_output(PORT_LEVLED1);
    nrf_gpio_cfg_output(PORT_LEVLED2);
    nrf_gpio_cfg_output(PORT_BATLED);
    nrf_gpio_cfg_output(PORT_PRELED);
    nrf_gpio_cfg_input(PORT_UART_RX, NRF_GPIO_PIN_NOPULL);
    nrf_gpio_cfg_output(PORT_UART_TX);
    nrf_gpio_cfg_input(PORT_PB, NRF_GPIO_PIN_NOPULL);
    nrf_gpio_cfg_input(PORT_PAD, NRF_GPIO_PIN_NOPULL);
    nrf_gpio_cfg_input(PORT_RAW_LIGHT, NRF_GPIO_PIN_NOPULL);
    nrf_gpio_cfg_input(PORT_AMP, NRF_GPIO_PIN_NOPULL);
    nrf_gpio_cfg_input(PORT_PRE_AMP, NRF_GPIO_PIN_NOPULL);
    nrf_gpio_cfg_output(PORT_AMP2);
    nrf_gpio_cfg_output(PORT_AMP1);

    // Turn off the LEDs
    hal_vLedOff(PORT_LEVLED1);
    hal_vLedOff(PORT_LEVLED2);
    hal_vLedOff(PORT_BATLED);
    hal_vLedOff(PORT_PRELED);
    nrf_gpio_cfg(PORT_SEN1, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_S0D1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(PORT_SEN2, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_S0D1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(PORT_SEN3, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_S0D1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_pin_set(PORT_SEN1);
    nrf_gpio_pin_set(PORT_SEN2);
    nrf_gpio_pin_set(PORT_SEN3);
    nrf_gpio_cfg(PORT_AMP1, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_S0D1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(PORT_AMP2, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_S0D1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_pin_clear(PORT_AMP1);
    nrf_gpio_pin_clear(PORT_AMP2);

    err_code = nrf_drv_gpiote_init();
    APP_ERROR_CHECK(err_code);
}

void hal_vLedOn(uint32_t u32Port)
{
    nrf_gpio_pin_clear(u32Port);
}

void hal_vLedOff(uint32_t u32Port)
{
    nrf_gpio_pin_set(u32Port);
}