// saadc.h
// SAADC management
// Author : G. KREBS
// Creation : 2019/05/08

typedef void (*daynightchange_handler_t)(uint8_t lvl);

void saadc_vInit(daynightchange_handler_t tDaynightchangeHandler);
void saadc_vTrig(void);
