typedef void (*detection_handler_t)(void);

void comp_vInit(detection_handler_t tDetectionHandler);
void comp_vEnable(bool bEnable);
void comp_setThreshold(uint8_t  threshold);