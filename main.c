/**
 * Copyright (c) 2014 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


// FILE : main.c

// *** INCLUDES ***
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nrf_dfu_ble_svci_bond_sharing.h"
#include "nrf_svci_async_function.h"
#include "nrf_svci_async_handler.h"

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "app_timer.h"
#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "bsp_btn_ble.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_state.h"
#include "ble_dfu.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "fds.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_clock.h"
#include "nrf_power.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_bootloader_info.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
//#include "low_power_pwm.h"
#include "app_pwm.h"

#include "definitions.h"
#include "hal.h"
#include "lptm_pushbutton.h"
#include "saadc.h"
#include "comp.h"

#include "ble_services.h"

#include "comp.h"

#include "lptm_flash.h"


//APP_PWM_INSTANCE(PWM1,1);                   // Create the instance "PWM1" using TIMER1.
//APP_PWM_INSTANCE(PWM2,1);                   // Create the instance "PWM2" using TIMER1.

static volatile bool ready_flag;            // A flag indicating PWM status.

/*void pwm1_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    ready_flag = true;
}

void pwm2_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    ready_flag = true;
}*/

BLE_SRV_DEF(mBleService); // BLE service instance
NRF_BLE_GATT_DEF(m_gatt);                                                       /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                         /**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising);                                             /**< Advertising module instance. */

static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;                        /**< Handle of the current connection. */
static void advertising_start(bool erase_bonds);

static int m_sensitivity = 0;

static bool m_isPrefocused = false;
static bool m_isInhibited = false;

//static ble_srv_t mBleService = {0};

static lptm_configuration_t m_config;
/*static low_power_pwm_t low_power_pwm_battled;
static low_power_pwm_t low_power_pwm_levled1;
static low_power_pwm_t low_power_pwm_levled2;
static low_power_pwm_t low_power_pwm_preled;*/

APP_TIMER_DEF(m_LightningTimeout_id);
APP_TIMER_DEF(m_LightningReactivate_id);

// UUID des services
static ble_uuid_t m_adv_uuids[] =  
{
    {BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}
};

/**
 * @brief Function to be called in timer interrupt.
 *
 * @param[in] p_context     General purpose pointer (unused).
 */
/*static void pwm_handler(void * p_context)
{
    uint8_t new_duty_cycle;
    static uint16_t led_0, led_1;
    uint32_t err_code;
    UNUSED_PARAMETER(p_context);

    low_power_pwm_t * pwm_instance = (low_power_pwm_t*)p_context;
}

static void pwm_init(void)
{
uint32_t err_code;
low_power_pwm_config_t low_power_pwm_config;

    // BATT LED
    APP_TIMER_DEF(lpp_timer_0);
    low_power_pwm_config.active_high    = false;
    low_power_pwm_config.period         = 220;
    low_power_pwm_config.bit_mask       = (1u << (uint32_t)((PORT_BATLED) & (~P0_PIN_NUM)));
    low_power_pwm_config.p_timer_id     = &lpp_timer_0;
    low_power_pwm_config.p_port         = NRF_GPIO;

    err_code = low_power_pwm_init((&low_power_pwm_battled), &low_power_pwm_config, pwm_handler);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_duty_set(&low_power_pwm_battled, 0);
    APP_ERROR_CHECK(err_code);

    // LEV LED 1
    APP_TIMER_DEF(lpp_timer_1);
    low_power_pwm_config.bit_mask       = (1u << (uint32_t)((PORT_LEVLED1) & (~P0_PIN_NUM)));
    low_power_pwm_config.p_timer_id     = &lpp_timer_1;

    err_code = low_power_pwm_init((&low_power_pwm_levled1), &low_power_pwm_config, pwm_handler);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_duty_set(&low_power_pwm_levled1, 0);
    APP_ERROR_CHECK(err_code);

    // LEV LED 2
    APP_TIMER_DEF(lpp_timer_2);
    low_power_pwm_config.bit_mask       = (1u << (uint32_t)((PORT_LEVLED2) & (~P0_PIN_NUM)));
    low_power_pwm_config.p_timer_id     = &lpp_timer_2;

    err_code = low_power_pwm_init((&low_power_pwm_levled2), &low_power_pwm_config, pwm_handler);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_duty_set(&low_power_pwm_levled2, 0);
    APP_ERROR_CHECK(err_code);

    // PRE LED
    APP_TIMER_DEF(lpp_timer_3);
    low_power_pwm_config.bit_mask       = (1u << (uint32_t)((PORT_PRELED) & (~P0_PIN_NUM)));
    low_power_pwm_config.p_timer_id     = &lpp_timer_3;

    err_code = low_power_pwm_init((&low_power_pwm_preled), &low_power_pwm_config, pwm_handler);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_duty_set(&low_power_pwm_preled, 0);
    APP_ERROR_CHECK(err_code);

    err_code = low_power_pwm_start((&low_power_pwm_battled), low_power_pwm_battled.bit_mask);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_start((&low_power_pwm_levled1), low_power_pwm_levled1.bit_mask);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_start((&low_power_pwm_levled2), low_power_pwm_levled2.bit_mask);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_start((&low_power_pwm_preled), low_power_pwm_preled.bit_mask);
    APP_ERROR_CHECK(err_code);
}

static void pwmLED_ON(low_power_pwm_t* pwmport)
{
uint32_t err_code;

    //err_code = low_power_pwm_duty_set(pwmport, 40);
    //APP_ERROR_CHECK(err_code);
}

static void pwmLED_OFF(low_power_pwm_t* pwmport)
{
uint32_t err_code;

    //err_code = low_power_pwm_duty_set(pwmport, 0);
    //APP_ERROR_CHECK(err_code);
}*/

static void setPrefocus(bool    bPrefocusActive)
{
    m_isPrefocused = bPrefocusActive;
    ble_srv_dataChange(m_conn_handle, &mBleService, (m_isPrefocused<<4)|(m_sensitivity));
    if(bPrefocusActive == false)
    {
        NRF_LOG_INFO("PREFOCUS OFF");
        //pwmLED_OFF(&low_power_pwm_preled);
        nrf_gpio_pin_set(PORT_PRELED);
        nrf_gpio_pin_clear(PORT_CTRL_FO);
    }
    else
    {
        NRF_LOG_INFO("PREFOCUS ON");
        //pwmLED_ON (&low_power_pwm_preled);
        nrf_gpio_pin_clear(PORT_PRELED);
        nrf_gpio_pin_set(PORT_CTRL_FO);
    }

    if(m_isPrefocused != m_config.isPrefocused)
    {
      m_config.isPrefocused = m_isPrefocused;
      lptmFlash_update(&m_config);
    }
}



static void togglePrefocus(void)
{
    if(m_isPrefocused == true)
    {
        setPrefocus(false);
    }
    else
    {
        setPrefocus(true);
    }
}

void setSensitivity(uint8_t u8Sensitivity)
{
    // Security
    if(u8Sensitivity >= 2)  m_sensitivity = 2;
    else                    m_sensitivity = u8Sensitivity;

    NRF_LOG_INFO("New sensitivity = %d", m_sensitivity);

    if(m_sensitivity != m_config.sensitivity)
    {
      m_config.sensitivity = m_sensitivity;
      lptmFlash_update(&m_config);
    }

    ble_srv_dataChange(m_conn_handle, &mBleService, (m_isPrefocused<<4)|(m_sensitivity));

    switch(m_sensitivity)
    {
        default:
        case 0:
            hal_vLedOff(PORT_LEVLED1);
            hal_vLedOff(PORT_LEVLED2);
            comp_setThreshold(30);
            //pwmLED_OFF(&low_power_pwm_levled1);
            //pwmLED_OFF(&low_power_pwm_levled2);
            //app_pwm_channel_duty_set(&PWM1, 0, 0);
            //app_pwm_channel_duty_set(&PWM1, 1, 0);
        break;
        case 1:
            hal_vLedOn (PORT_LEVLED1);
            hal_vLedOff(PORT_LEVLED2);
            comp_setThreshold(7);
            //pwmLED_ON (&low_power_pwm_levled1);
            //pwmLED_OFF(&low_power_pwm_levled2);
            //app_pwm_channel_duty_set(&PWM1, 0, 25);
            //app_pwm_channel_duty_set(&PWM1, 1, 0);
        break;
        case 2:
            hal_vLedOn (PORT_LEVLED1);
            hal_vLedOn (PORT_LEVLED2);
            comp_setThreshold(2);
            //pwmLED_ON(&low_power_pwm_levled1);
            //pwmLED_ON(&low_power_pwm_levled2);
            //app_pwm_channel_duty_set(&PWM1, 0, 25);
            //app_pwm_channel_duty_set(&PWM1, 1, 25);
        break;
    }
}

// *** Handler for shutdown preparation ***
//  During shutdown procedures, this function will be called at a 1 second interval
//   until the function returns true. When the function returns true, it means that the
//   app is ready to reset to DFU mode.
static bool app_shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
uint32_t err_code;
    switch (event)
    {
        case NRF_PWR_MGMT_EVT_PREPARE_DFU:
            //NRF_LOG_INFO("Power management wants to reset to DFU mode.");
            // YOUR_JOB: Get ready to reset into DFU mode
            //
            // If you aren't finished with any ongoing tasks, return "false" to
            // signal to the system that reset is impossible at this stage.
            //
            // Here is an example using a variable to delay resetting the device.
            //
            //if (!m_ready_for_reset)
            //{
            //      return false;
            //}
            //else
            //{
            
            // Device ready to enter
                
               // err_code = sd_softdevice_disable();
               // APP_ERROR_CHECK(err_code);
               // err_code = app_timer_stop_all();
               // APP_ERROR_CHECK(err_code);
            //}
            break;

        default:
            // YOUR_JOB: Implement any of the other events available from the power management module:
            //      -NRF_PWR_MGMT_EVT_PREPARE_SYSOFF
            //      -NRF_PWR_MGMT_EVT_PREPARE_WAKEUP
            //      -NRF_PWR_MGMT_EVT_PREPARE_RESET
            return true;
    }

    NRF_LOG_INFO("Power management allowed to reset to DFU mode.");
    return true;
}

// Register application shutdown handler with priority 0.
NRF_PWR_MGMT_HANDLER_REGISTER(app_shutdown_handler, 0);


static void buttonless_dfu_sdh_state_observer(nrf_sdh_state_evt_t state, void * p_context)
{
    if (state == NRF_SDH_EVT_STATE_DISABLED)
    {
        // Softdevice was disabled before going into reset. Inform bootloader to skip CRC on next boot.
        nrf_power_gpregret2_set(BOOTLOADER_DFU_SKIP_CRC);

        //Go to system off.
        nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
    }
}

/* nrf_sdh state observer. */
NRF_SDH_STATE_OBSERVER(m_buttonless_dfu_state_obs, 0) =
{
    .handler = buttonless_dfu_sdh_state_observer,
};

static void advertising_config_get(ble_adv_modes_config_t * p_config)
{
    memset(p_config, 0, sizeof(ble_adv_modes_config_t));

    p_config->ble_adv_fast_enabled  = true;
    p_config->ble_adv_fast_interval = APP_ADV_INTERVAL;
    p_config->ble_adv_fast_timeout  = APP_ADV_DURATION;
}

static void disconnect(uint16_t conn_handle, void * p_context)
{
    UNUSED_PARAMETER(p_context);

    ret_code_t err_code = sd_ble_gap_disconnect(conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    if (err_code != NRF_SUCCESS)
    {
        //NRF_LOG_WARNING("Failed to disconnect connection. Connection handle: %d Error: %d", conn_handle, err_code);
    }
    else
    {
        //NRF_LOG_DEBUG("Disconnected connection handle %d", conn_handle);
    }
}



// YOUR_JOB: Update this code if you want to do anything given a DFU event (optional).
// Function for handling dfu events from the Buttonless Secure DFU service
//  [IN] event : Event from the Buttonless Secure DFU service
static void ble_dfu_evt_handler(ble_dfu_buttonless_evt_type_t event)
{
    switch (event)
    {
        case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
        {
            NRF_LOG_INFO("Device is preparing to enter bootloader mode.");

            // Prevent device from advertising on disconnect.
            ble_adv_modes_config_t config;
            advertising_config_get(&config);
            config.ble_adv_on_disconnect_disabled = true;
            ble_advertising_modes_config_set(&m_advertising, &config);

            // Disconnect all other bonded devices that currently are connected.
            // This is required to receive a service changed indication
            // on bootup after a successful (or aborted) Device Firmware Update.
            uint32_t conn_count = ble_conn_state_for_each_connected(disconnect, NULL);
            NRF_LOG_INFO("Disconnected %d links.", conn_count);
            break;
        }

        case BLE_DFU_EVT_BOOTLOADER_ENTER:
            // YOUR_JOB: Write app-specific unwritten data to FLASH, control finalization of this
            //           by delaying reset by reporting false in app_shutdown_handler
            NRF_LOG_INFO("Device will enter bootloader mode.");
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
            NRF_LOG_ERROR("Request to enter bootloader mode failed asynchroneously.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            break;

        case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
            NRF_LOG_ERROR("Request to send a response to client failed.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            APP_ERROR_CHECK(false);
            break;

        default:
            NRF_LOG_ERROR("Unknown event from ble_dfu_buttonless.");
            break;
    }
}


// Callback function for asserts in the SoftDevice.
//   This function will be called in case of an assert in the SoftDevice.
//          !!!!  This handler is an example only and does not fit a final product. You need to analyze how your product is supposed to react in case of Assert.
//          !!!!  On assert from the SoftDevice, the system can only recover on reset.
//  [IN] line_num  : Line number of the failing ASSERT call.
//  [IN] file_name : File name of the failing ASSERT call.
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

// Function for handling Peer Manager events.
//  [IN] p_evt : Peer Manager event.
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    pm_handler_on_pm_evt(p_evt);
    pm_handler_flash_clean(p_evt);

    /*switch (p_evt->evt_id)
    {
        case PM_EVT_PEERS_DELETE_SUCCEEDED:
            advertising_start(false);
            break;

        default:
            break;
    }*/
}


// Function for the Timer initialization.
// Initializes the timer module. This creates and starts application timers.
static void timers_init(void)
{
    // Init clocks
    /*ret_code_t err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_clock_lfclk_request(NULL);*/

    // Initialize timer module.
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}


// Function for the GAP initialization.
//   This function sets up all the necessary GAP (Generic Access Profile) parameters of the
//   device including the device name, appearance, and the preferred connection parameters.
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    /* YOUR_JOB: Use an appearance value matching the application's use case.
       err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_);
       APP_ERROR_CHECK(err_code); */

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


// Function for initializing the GATT module.
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}


// Function for handling Queued Write Module errors.
//   A pointer to this function will be passed to each service which may need to inform the
//   application about an error.
// [IN] nrf_error : Error code containing information about what went wrong.
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

// Function for handling write events to the prefocus characteristic.
static void data_write_handler(uint16_t conn_handle, ble_srv_t * p_lbs, uint8_t    data)
{
    NRF_LOG_INFO("Data received : %x", data);
    setPrefocus((data&0xF0)!=0?true:false);
    setSensitivity(data&0xF);
}


// Function for initializing services that will be used by the application.
static void services_init(void)
{
  ret_code_t         err_code;
  nrf_ble_qwr_init_t qwr_init = {0};
  ble_dfu_buttonless_init_t dfus_init = {0};
  ble_lpt_init_t init = {0};


    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    dfus_init.evt_handler = ble_dfu_evt_handler;

    err_code = ble_dfu_buttonless_init(&dfus_init);
    APP_ERROR_CHECK(err_code);

    // Init service
    init.data_write_handler = data_write_handler;
    ble_services_init(&mBleService, &init, FW_VERSION);
}


// Function for handling the Connection Parameters Module.
//   This function will be called for all events in the Connection Parameters Module which
//   are passed to the application.
//
//  Note : All this function does is to disconnect. This could have been done by simply
//         setting the disconnect_on_fail config parameter, but instead we use the event
//         handler mechanism to demonstrate its use.
//
// [IN] p_evt : Event received from the Connection Parameters Module.
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}

// Function for handling a Connection Parameters error.
//  [IN] nrf_error  Error code containing information about what went wrong.
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


// Function for initializing the Connection Parameters module.
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

// Function for putting the chip into sleep mode.
//  Note : This function will not return.
static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    //err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    //APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    //Disable SoftDevice. It is required to be able to write to GPREGRET2 register (SoftDevice API blocks it).
    //GPREGRET2 register holds the information about skipping CRC check on next boot.
    err_code = nrf_sdh_disable_request();
    APP_ERROR_CHECK(err_code);
}

// Function for handling advertising events.
//   This function will be called for advertising events which are passed to the application.
//  [IN] ble_adv_evt : Advertising event.
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    ret_code_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            //NRF_LOG_INFO("Fast advertising.");
            //err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            //APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_IDLE:
            sleep_mode_enter();
            break;

        default:
            break;
    }
}


// Function for handling BLE events.
//  [IN] p_ble_evt : Bluetooth stack event.
//  [IN] p_context : Unused.
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code = NRF_SUCCESS;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_DISCONNECTED:
            //NRF_LOG_INFO("Disconnected.");
            // LED indication will be changed when advertising starts.
            break;

        case BLE_GAP_EVT_CONNECTED:
            //NRF_LOG_INFO("Connected.");
            //err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            //APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            //NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
            break;
        }

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            //NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            //NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}


// Function for initializing the BLE stack.
//   Initializes the SoftDevice and the BLE event interrupt.
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);
    

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


// Function for the Peer Manager initialization.
static void peer_manager_init(void)
{
ble_gap_sec_params_t sec_param;
ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}


// Clear bonding information from persistent storage
static void delete_bonds(void)
{
    ret_code_t err_code;

    //NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}



// Function for handling events from the BSP module.
//  [IN] event : Event generated when button is pressed.
static void bsp_event_handler(bsp_event_t event)
{
    ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            sleep_mode_enter();
            break; // BSP_EVENT_SLEEP

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break; // BSP_EVENT_DISCONNECT

        case BSP_EVENT_WHITELIST_OFF:
            if (m_conn_handle == BLE_CONN_HANDLE_INVALID)
            {
                err_code = ble_advertising_restart_without_whitelist(&m_advertising);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
            }
            break;
        default:
            break;
    }
}

// Function for initializing buttons and leds.
static void buttons_leds_init(bool * p_erase_bonds)
{
    ret_code_t err_code;
    bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LEDS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}


// Function for initializing the Advertising functionality.
static void advertising_init(void)
{
    ret_code_t             err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;

    advertising_config_get(&init.config);

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

// Function for initializing the nrf log module.
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

// Function for starting advertising.
static void advertising_start(bool erase_bonds)
{
    if (erase_bonds == true)
    {
        delete_bonds();
        // Advertising is started by PM_EVT_PEERS_DELETED_SUCEEDED event
    }
    else
    {
        ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
        APP_ERROR_CHECK(err_code);
    }
}

static void lightning_detected(void)
{
    if(!m_isInhibited)
    {
        comp_vEnable(false);
        m_isInhibited = true;
        NRF_LOG_INFO("LIGHTNING!!");
        //nrf_drv_gpiote_in_event_disable(PORT_AMP);
        nrf_gpio_pin_set(PORT_CTRL_FO);
        nrf_gpio_pin_set(PORT_CTRL_SH);

        // Start timer end
        app_timer_start(m_LightningTimeout_id, APP_TIMER_TICKS(1000), NULL);  // Inhibition time
    }
}

void Lightning_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    lightning_detected();
}

// Lightning
void GPIOTE_LightningConfig(void)
{
ret_code_t l_tError;
nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_LOTOHI(true);

    in_config.pull = NRF_GPIO_PIN_PULLUP;

    l_tError = nrf_drv_gpiote_in_init(PORT_AMP, &in_config, Lightning_handler);
    APP_ERROR_CHECK(l_tError);
}


void vIncreaseSensitivity(void)
{
    if(m_sensitivity < 2) m_sensitivity++;
    else                  m_sensitivity = 0;

    setSensitivity(m_sensitivity);    
}

static void lightningTimer(void* p_context)
{
    NRF_LOG_INFO("STOP SHUTTER");
    nrf_gpio_pin_clear(PORT_CTRL_SH);
    nrf_gpio_pin_clear(PORT_CTRL_FO);
    app_timer_start(m_LightningReactivate_id, APP_TIMER_TICKS(100), NULL);  // Inhibition time
}

static void lightningReactivate(void* p_context)
{
    NRF_LOG_INFO("REACTIVATE LIGHTNING");
    if(m_isPrefocused)
    {
        nrf_gpio_pin_set(PORT_CTRL_FO);
    }
    
    m_isInhibited = false;
    comp_vEnable(true);
}

static void daynightChangeHandler(uint8_t lvl)
{
  NRF_LOG_INFO("DAY/NIGHT change");
  comp_vEnable(false);
  m_isInhibited = true;
  m_config.dayNightLevel = lvl;
  lptmFlash_update(&m_config);
  app_timer_start(m_LightningReactivate_id, APP_TIMER_TICKS(200), NULL);  // Inhibition time
}


// *** MAIN FUNCTION ***
int main(void)
{
bool       l_bEraseBonds;
ret_code_t l_tErrorCode;

    // Log
    log_init();

    NRF_LOG_INFO("\n\n");

    hal_vInit();

    

    timers_init();
    
    // Init buttons&leds
    buttons_leds_init(&l_bEraseBonds);

    // Initialize the async SVCI interface to bootloader before any interrupts are enabled.
    //l_tErrorCode = ble_dfu_buttonless_async_svci_init();
    //APP_ERROR_CHECK(l_tErrorCode);
    
    l_tErrorCode = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(l_tErrorCode);

    ble_stack_init();
    sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);
    sd_power_mode_set(NRF_POWER_MODE_LOWPWR);
    peer_manager_init();
    gap_params_init();
    gatt_init();
    advertising_init();
    services_init();
    conn_params_init();

    //pwm_init();
    /*app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_2CH(5000L, PORT_LEVLED1, PORT_LEVLED2);
    app_pwm_config_t pwm2_cfg = APP_PWM_DEFAULT_CONFIG_2CH(5000L, PORT_BATLED, PORT_PRELED);
    // Initialize and enable PWM.
    l_tErrorCode = app_pwm_init(&PWM1,&pwm1_cfg,pwm1_ready_callback);
    APP_ERROR_CHECK(l_tErrorCode);
    app_pwm_enable(&PWM1);
    l_tErrorCode = app_pwm_init(&PWM2,&pwm2_cfg,pwm2_ready_callback);
    APP_ERROR_CHECK(l_tErrorCode);
    app_pwm_enable(&PWM2);
    app_pwm_channel_duty_set(&PWM1, 0, 0);
    app_pwm_channel_duty_set(&PWM1, 1, 0);
    app_pwm_channel_duty_set(&PWM2, 0, 0);
    app_pwm_channel_duty_set(&PWM2, 1, 0);*/

    saadc_vInit(daynightChangeHandler);

    m_isInhibited = true;
    comp_vInit(lightning_detected);

    // FDS
    lptmFlash_init();
    lptmFlash_read(&m_config);
    setSensitivity(m_config.sensitivity);
    setPrefocus(m_config.isPrefocused);
    m_config.boot_count++;
    lptmFlash_update(&m_config);
    

    NRF_LOG_INFO("LPTM3 application (%s %s)", __DATE__, __TIME__);
    NRF_LOG_FLUSH();
    
    

    // Config push button
    lptm_pushbutton_init(PORT_PB);
    lptm_pushbutton_setShortPressAction(vIncreaseSensitivity);
    lptm_pushbutton_setLongPressAction(togglePrefocus);

    // Lightning trig
    GPIOTE_LightningConfig();

    nrf_drv_gpiote_in_event_enable(PORT_AMP, true);

    // Create app timer
    // Detection timeout
    l_tErrorCode = app_timer_create(&m_LightningTimeout_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                lightningTimer);

    // Inhibition end
    l_tErrorCode = app_timer_create(&m_LightningReactivate_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                lightningReactivate);

    advertising_start(l_bEraseBonds);

    m_isInhibited = false;

    // Enter main loop.
    while(1)
    {
        NRF_LOG_FLUSH();
        (void) sd_app_evt_wait();
    }
}
