// saadc.c
// SAADC management
// Author : G. KREBS
// Creation : 2019/05/08

#include "nrf_drv_saadc.h"
#include "nrf_drv_gpiote.h"
#include "nrf_log.h"
#include "app_error.h"
#include "hal.h"
#include "saadc.h"
#include "app_timer.h"

#define SAMPLES_IN_BUFFER 2
volatile uint8_t state = 1;

static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];
static uint32_t              m_adc_evt_counter;

static uint8_t  g_u8SensitivityLevel = 0;

APP_TIMER_DEF(m_sensChangeTmr_id);
APP_TIMER_DEF(m_trigTimerID);

void    (*g_tDaynightchangeHandler)(uint8_t lvl);

void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
uint8_t l_u8SensitivityChanged = false;
uint32_t  l_u32BattLvlmV = 0;

    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);

        //NRF_LOG_INFO("RAW light = %dmV", p_event->data.done.p_buffer[0]*3000/255);

        l_u32BattLvlmV = p_event->data.done.p_buffer[1]*3600/255;
        //NRF_LOG_INFO("Batt lvl = %dmV", l_u32BattLvlmV);

        if(l_u32BattLvlmV < 2000) nrf_gpio_pin_clear(PORT_BATLED);

     
        //NRF_LOG_INFO("ADC event number: %d", (int)m_adc_evt_counter);

        if((p_event->data.done.p_buffer[0] > 100) && (g_u8SensitivityLevel < 3))
        {
            g_u8SensitivityLevel++;
            l_u8SensitivityChanged = true;
        }
        else if((p_event->data.done.p_buffer[0] < 10) && (g_u8SensitivityLevel > 0))
        {
            g_u8SensitivityLevel--;
            l_u8SensitivityChanged = true;
        }

        if(l_u8SensitivityChanged == true)
        {
            //nrf_drv_gpiote_in_event_disable(PORT_AMP);
            NRF_LOG_INFO("LVL%d", g_u8SensitivityLevel);
            g_tDaynightchangeHandler(g_u8SensitivityLevel);

            switch(g_u8SensitivityLevel)
            {
                case 0:
                default:
                    nrf_gpio_pin_set(PORT_SEN1);
                    nrf_gpio_pin_set(PORT_SEN2);
                    nrf_gpio_pin_set(PORT_SEN3);
                    break;
                case 1:
                    nrf_gpio_pin_set(PORT_SEN1);
                    nrf_gpio_pin_set(PORT_SEN2);
                    nrf_gpio_pin_clear(PORT_SEN3);
                    break;
                case 2:
                    nrf_gpio_pin_set(PORT_SEN1);
                    nrf_gpio_pin_clear(PORT_SEN2);
                    nrf_gpio_pin_clear(PORT_SEN3);
                    break;
                case 3:
                    nrf_gpio_pin_clear(PORT_SEN1);
                    nrf_gpio_pin_clear(PORT_SEN2);
                    nrf_gpio_pin_clear(PORT_SEN3);
                    break;
            }

            // Reactivate lightning trigger interrupt in 100ms
            app_timer_start(m_sensChangeTmr_id, APP_TIMER_TICKS(100), NULL);
        }
        m_adc_evt_counter++;
    }
}

void saadc_vTrig(void)
{
    ret_code_t err_code;
	//Event handler is called immediately after conversion is finished.
	err_code = nrf_drv_saadc_sample(); // Check error
	APP_ERROR_CHECK(err_code);
}

static void trigTimer(void* p_context)
{
    saadc_vTrig();
}

static void sensFilteringTimer(void* p_context)
{
    nrf_drv_gpiote_in_event_enable(PORT_AMP, true);
}


void saadc_vInit(daynightchange_handler_t tDaynightchangeHandler)
{
ret_code_t err_code;
nrf_saadc_channel_config_t  l_tRawLightConfig = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN6);
nrf_saadc_channel_config_t  l_tBattLvlConfig = {
                                                    .resistor_p = NRF_SAADC_RESISTOR_DISABLED,
                                                    .resistor_n = NRF_SAADC_RESISTOR_DISABLED,
                                                    .gain       = NRF_SAADC_GAIN1_6,
                                                    .reference  = NRF_SAADC_REFERENCE_INTERNAL,
                                                    .acq_time   = NRF_SAADC_ACQTIME_40US,
                                                    .mode       = NRF_SAADC_MODE_SINGLE_ENDED,
                                                    .burst      = NRF_SAADC_BURST_ENABLED,
                                                    .pin_p      = NRF_SAADC_INPUT_VDD,
                                                    .pin_n      = NRF_SAADC_INPUT_DISABLED
                                                };

    // Config detection function
    g_tDaynightchangeHandler = tDaynightchangeHandler;

    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(0, &l_tRawLightConfig);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(1, &l_tBattLvlConfig);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);

    // Create timer to re-activate lightning trig
    err_code = app_timer_create(&m_sensChangeTmr_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                sensFilteringTimer);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_trigTimerID,
                                APP_TIMER_MODE_REPEATED,
                                trigTimer);
    APP_ERROR_CHECK(err_code);

    // Start trig timer (loop every 2s)
    app_timer_start(m_trigTimerID, APP_TIMER_TICKS(2000), NULL);


    NRF_LOG_INFO("SAADC started");
}