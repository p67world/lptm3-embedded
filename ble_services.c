// Services BLE
#include "app_error.h"
#include "nrf_log.h"
#include "sdk_common.h"
#include "ble_services.h"
#include "ble_srv_common.h"
#include "ble_dfu.h"

#include "nrf_drv_gpiote.h"

// Private functions
static void on_write(ble_srv_t*  ptService, ble_evt_t const* ptBleEvent)
{
ble_gatts_evt_write_t const* l_ptEventWrite = &ptBleEvent->evt.gatts_evt.params.write;

    if(     (l_ptEventWrite->handle == ptService->data_char_handle.value_handle)
        &&  (l_ptEventWrite->len == 1)
        &&  (ptService->data_write_handler != NULL))
        {
            ptService->data_write_handler(ptBleEvent->evt.gap_evt.conn_handle, ptService, l_ptEventWrite->data[0]);
        }
}

void add_notif_characteristic(uint16_t uuid, ble_srv_t* ptService, ble_gatts_char_handles_t *handle)
{
uint32_t            u32ErrorCode;
ble_uuid_t          charUuid;
ble_gatts_attr_md_t attr_md;
ble_gatts_attr_t    attr_char_value;
uint8_t             dummyVal = 0;
ble_gatts_char_md_t char_md;

    charUuid.uuid = uuid;
    memset(&attr_md, 0, sizeof(attr_md));
    attr_md.vloc        = BLE_GATTS_VLOC_STACK;
    memset(&attr_char_value, 0, sizeof(attr_char_value));  
    attr_char_value.p_uuid      = &charUuid;
    attr_char_value.p_attr_md   = &attr_md;
    attr_char_value.max_len     = sizeof(dummyVal);
    attr_char_value.init_len    = sizeof(dummyVal);
    attr_char_value.p_value     = &dummyVal;
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.read = 1;
    char_md.char_props.write = 1;
    char_md.p_cccd_md           = &attr_md;
    char_md.char_props.notify = 1;
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    u32ErrorCode = sd_ble_gatts_characteristic_add(ptService->service_handle,
                                   &char_md,
                                   &attr_char_value,
                                   handle);
    APP_ERROR_CHECK(u32ErrorCode);

}

// Public functions
void ble_services_init(ble_srv_t* ptService, const ble_lpt_init_t* p_lpt_init, uint8_t fwVersion)
{
uint32_t                u32ErrorCode;
ble_uuid128_t           tBleBaseUuid = BLE_UUID_LPT_BASE_UUID;
ble_uuid_t              tBleServiceUuid;
ble_uuid_t              tPreCharUuid;
ble_uuid_t              tSensCharUuid;
ble_add_char_params_t   tAddCharParams;
ble_gatts_hvx_params_t  l_tParams;
uint16_t                l_u16Len = sizeof(fwVersion);
uint8_t                 l_u8DummyVal = 0;
ble_gatts_attr_md_t attr_md;
ble_gatts_attr_t    attr_char_value;
ble_gatts_char_md_t char_md;

    // Initialize service structure
    ptService->data_write_handler = p_lpt_init->data_write_handler;

    // Add service
    tBleServiceUuid.uuid = BLE_UUID_LPT_SERVICE;
    u32ErrorCode = sd_ble_uuid_vs_add(&tBleBaseUuid, &tBleServiceUuid.type);
    APP_ERROR_CHECK(u32ErrorCode);

    u32ErrorCode = sd_ble_gatts_service_add (BLE_GATTS_SRVC_TYPE_PRIMARY,
                                            &tBleServiceUuid,
                                            &ptService->service_handle);
    APP_ERROR_CHECK(u32ErrorCode);

    // Add data characteristic
    add_notif_characteristic(BLE_UUID_LPT_DATA_CHAR, ptService, &ptService->data_char_handle);
 
    // Add FW version characteristic
    memset(&tAddCharParams, 0, sizeof(tAddCharParams));
    tAddCharParams.uuid             = BLE_UUID_LPT_FWVERSION_CHAR;
    tAddCharParams.uuid_type        = ptService->uuid_type;
    tAddCharParams.init_len         = sizeof(uint8_t);
    tAddCharParams.max_len          = sizeof(uint8_t);
    tAddCharParams.char_props.read  = 1;
    tAddCharParams.read_access      = SEC_OPEN;
    tAddCharParams.p_init_value     = &fwVersion;

    u32ErrorCode = characteristic_add(  ptService->service_handle,
                                        &tAddCharParams,
                                        &ptService->fwversion_char_handle);
    

                                        
    if(u32ErrorCode != NRF_SUCCESS)
    {
        NRF_LOG_ERROR("characteristic error %d", u32ErrorCode);
    }
}

uint32_t ble_srv_dataChange(uint16_t   u16Handle, ble_srv_t* ptService, uint8_t u8Data)
{
ble_gatts_hvx_params_t  l_tParams;
uint16_t                l_u16Len = 1; // 1 

    memset(&l_tParams, 0, sizeof(l_tParams));
    l_tParams.type      = BLE_GATT_HVX_NOTIFICATION;
    l_tParams.handle    = ptService->data_char_handle.value_handle;
    l_tParams.p_data    = &u8Data;
    l_tParams.p_len     = &l_u16Len;

    return sd_ble_gatts_hvx(u16Handle, &l_tParams);
}


void ble_srv_on_ble_evt(ble_evt_t const* ptBleEvent, void* p_context)
{
ble_srv_t*  l_ptService = (ble_srv_t*)p_context;

    switch(ptBleEvent->header.evt_id)
    {
        case BLE_GATTS_EVT_WRITE:
            on_write(l_ptService, ptBleEvent);
            break;
        default:
            break;
    }
}