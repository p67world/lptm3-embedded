#ifndef BLE_SERVICES_H
#define BLE_SERVICES_H
// Services BLE

#include <stdint.h>
#include "ble.h"
#include "ble_srv_common.h"

// Macros
#define BLE_SRV_DEF(_name)                                                                          \
static ble_srv_t _name;                                                                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_LPT_BLE_OBSERVER_PRIO,                                                     \
                     ble_srv_on_ble_evt, &_name)


#define BLE_UUID_LPT_BASE_UUID      {0x6E, 0x65, 0x62, 0x75, 0x6C, 0x65, 0x6F, 0x20, 0x6C, 0x70, 0x74, 0x20, 0x73, 0x72, 0x76, 0x20} // 128-bit base UUID
#define BLE_UUID_LPT_SERVICE        0x6E01  // LPT service
#define BLE_UUID_LPT_DATA_CHAR      0x6E02  // Base values characteristic
#define BLE_UUID_LPT_FWVERSION_CHAR 0x6E04  // Sensitivity characteristic

// Typedefs
typedef struct ble_srv_s ble_srv_t;
typedef void (*ble_srv_data_write_handler_t)    (uint16_t conn_handle, ble_srv_t * p_lbs, uint8_t data);

// LPT service init structure
//  Contains all options and data needed for initialization of the service
typedef struct
{
  ble_srv_data_write_handler_t data_write_handler;  // Event handler to be called when the LPT data characteristic is written
}ble_lpt_init_t;

// Service structure
struct ble_srv_s
{
    uint16_t                            service_handle;
    ble_gatts_char_handles_t            data_char_handle;
    ble_gatts_char_handles_t            fwversion_char_handle;
    uint8_t                             uuid_type;
    ble_srv_data_write_handler_t        data_write_handler;
};


// Prototype
void ble_services_init(ble_srv_t* p_service, const ble_lpt_init_t* p_lpt_init, uint8_t fwVersion);
uint32_t ble_srv_dataChange(uint16_t   u16Handle, ble_srv_t* ptService, uint8_t u8Data);
void ble_srv_on_ble_evt(ble_evt_t const* ptBleEvent, void* p_context);

#endif
