// COMP
#include "nrf_drv_comp.h"
#include "app_error.h"
#include "nrf_log.h"
#include "comp.h"

static nrf_drv_comp_config_t    g_tConfig = NRF_DRV_COMP_DEFAULT_CONFIG(NRF_COMP_INPUT_4);

#define DEF_THRESHOLD_UP    64

void    (*g_tDetectionHandler)(void);
static bool m_bEnable = true;

static void comp_event_handler(nrf_comp_event_t tEvent)
{
    switch(tEvent)
    {
        case NRF_COMP_EVENT_UP:
            //NRF_LOG_INFO("DETECT");
            g_tDetectionHandler();
            break;
        default:
            break;
    }
}

void comp_vEnable(bool bEnable)
{
  if(bEnable == false)
  {
    nrf_drv_comp_stop();
  }
  else
  {
    nrf_drv_comp_start(NRF_DRV_COMP_EVT_EN_UP_MASK, NULL);
  }
}

void comp_setThreshold(uint8_t  threshold)
{
uint32_t    l_u32ErrorCode;
nrf_comp_th_t th;

  th.th_up = threshold;
  th.th_down = g_tConfig.threshold.th_down;

  comp_vEnable(false);
  nrf_comp_th_set(th);
  comp_vEnable(true);

  //nrf_drv_comp_stop();
 /* nrf_drv_comp_uninit();
  g_tConfig.threshold.th_up = threshold;
  l_u32ErrorCode = nrf_drv_comp_init(&g_tConfig, comp_event_handler);
  APP_ERROR_CHECK(l_u32ErrorCode);
  nrf_drv_comp_start(NRF_DRV_COMP_EVT_EN_UP_MASK, NULL);*/
}

void comp_vInit(void (*tDetectionHandler)(void))
{
uint32_t    l_u32ErrorCode;

    // Threshold
    g_tConfig.threshold.th_up = DEF_THRESHOLD_UP;
    g_tConfig.reference = NRF_COMP_REF_Int1V2;

    // Config detection function
    g_tDetectionHandler = tDetectionHandler;

    // Initialize
    l_u32ErrorCode = nrf_drv_comp_init(&g_tConfig, comp_event_handler);
    APP_ERROR_CHECK(l_u32ErrorCode);
    comp_vEnable(true);
}