typedef void (*pushbutton_action_t)(void);

void lptm_pushbutton_init(uint32_t port);
void lptm_pushbutton_setShortPressAction(pushbutton_action_t actionFunction);
void lptm_pushbutton_setLongPressAction(pushbutton_action_t actionFunction);