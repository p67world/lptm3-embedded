#include "fds.h"
#include "nrf_log.h"
#include "app_error.h"
#include "definitions.h"
#include "lptm_flash.h"


#define CONFIG_FILE     (0x1111)
#define CONFIG_REC_KEY  (0x2222)

static bool volatile m_fds_initialized;
static fds_record_desc_t    m_desc = {0};
static fds_find_token_t     m_token = {0};

static lptm_configuration_t  m_default_config =
{
    .boot_count     = 0,
    .device_name    = DEVICE_NAME,
    .sensitivity    = DEFAULT_SENSITIVITY,
    .isPrefocused   = DEFAULT_PREFOCUS,
    .dayNightLevel  = DEFAULT_DAYNIGHTLVL,
};


static fds_record_t const m_default_config_record = 
{
    .file_id    = CONFIG_FILE,
    .key        = CONFIG_REC_KEY,
    .data.p_data    = &m_default_config,
    .data.length_words = (sizeof(m_default_config) + 3) / sizeof(uint32_t),
};

/* Array to map FDS return values to strings. */
char const * fds_err_str[] =
{
    "FDS_SUCCESS",
    "FDS_ERR_OPERATION_TIMEOUT",
    "FDS_ERR_NOT_INITIALIZED",
    "FDS_ERR_UNALIGNED_ADDR",
    "FDS_ERR_INVALID_ARG",
    "FDS_ERR_NULL_ARG",
    "FDS_ERR_NO_OPEN_RECORDS",
    "FDS_ERR_NO_SPACE_IN_FLASH",
    "FDS_ERR_NO_SPACE_IN_QUEUES",
    "FDS_ERR_RECORD_TOO_LARGE",
    "FDS_ERR_NOT_FOUND",
    "FDS_ERR_NO_PAGES",
    "FDS_ERR_USER_LIMIT_REACHED",
    "FDS_ERR_CRC_CHECK_FAILED",
    "FDS_ERR_BUSY",
    "FDS_ERR_INTERNAL",
};

/* Array to map FDS events to strings. */
static char const * fds_evt_str[] =
{
    "FDS_EVT_INIT",
    "FDS_EVT_WRITE",
    "FDS_EVT_UPDATE",
    "FDS_EVT_DEL_RECORD",
    "FDS_EVT_DEL_FILE",
    "FDS_EVT_GC",
};

static void fds_evt_handler(fds_evt_t const * p_evt)
{
    NRF_LOG_INFO("Event: %s received (%s)",
                  fds_evt_str[p_evt->id],
                  fds_err_str[p_evt->result]);

    switch (p_evt->id)
    {
        case FDS_EVT_INIT:
            if (p_evt->result == FDS_SUCCESS)
            {
                m_fds_initialized = true;
            }
            break;

        case FDS_EVT_WRITE:
        {
            if (p_evt->result == FDS_SUCCESS)
            {
                NRF_LOG_INFO("Record ID:\t0x%04x",  p_evt->write.record_id);
                NRF_LOG_INFO("File ID:\t0x%04x",    p_evt->write.file_id);
                NRF_LOG_INFO("Record key:\t0x%04x", p_evt->write.record_key);
            }
        } break;

        case FDS_EVT_DEL_RECORD:
        {
            if (p_evt->result == FDS_SUCCESS)
            {
                NRF_LOG_INFO("Record ID:\t0x%04x",  p_evt->del.record_id);
                NRF_LOG_INFO("File ID:\t0x%04x",    p_evt->del.file_id);
                NRF_LOG_INFO("Record key:\t0x%04x", p_evt->del.record_key);
            }
        } break;

        default:
            break;
    }
}

/**@brief   Wait for fds to initialize. */
static void wait_for_fds_ready(void)
{
    while (!m_fds_initialized)
    {
        (void) sd_app_evt_wait();
    }
}

void lptmFlash_init(void)
{
ret_code_t  error_code;

    // Register first to receive an event when initialization is completed
    (void) fds_register(fds_evt_handler);

    error_code = fds_init();
    APP_ERROR_CHECK(error_code);

    // Wait for FDS to initialize
    wait_for_fds_ready();
}

void lptmFlash_update(lptm_configuration_t const *configuration)
{
fds_record_desc_t desc = {0};
fds_find_token_t  ftok = {0};

    fds_gc();

    if (fds_record_find(CONFIG_FILE, CONFIG_REC_KEY, &desc, &ftok) == FDS_SUCCESS)
    {
        fds_record_t const rec =
        {
            .file_id           = CONFIG_FILE,
            .key               = CONFIG_REC_KEY,
            .data.p_data       = configuration,
            .data.length_words = (sizeof(lptm_configuration_t) + 3) / sizeof(uint32_t)
        };

        ret_code_t rc = fds_record_update(&desc, &rec);
        if (rc != FDS_SUCCESS)
        {
            NRF_LOG_INFO("error: fds_record_update() returned %s", fds_err_str[rc]);
        }
    }
    else
    {
        NRF_LOG_INFO("error: could not find config file");
    }

    
}

void lptmFlash_read(lptm_configuration_t *configuration)
{
ret_code_t error_code;
fds_record_desc_t desc = {0};
fds_find_token_t  ftok = {0};

    if (fds_record_find(CONFIG_FILE, CONFIG_REC_KEY, &desc, &ftok) == FDS_SUCCESS)
    {
        fds_flash_record_t frec = {0};

        error_code = fds_record_open(&desc, &frec);       

        memcpy(configuration, (lptm_configuration_t *)frec.p_data, sizeof(lptm_configuration_t));

        error_code=fds_record_close(&desc);
        APP_ERROR_CHECK(error_code);
    }
    else
    {
        memcpy(configuration, &m_default_config, sizeof(lptm_configuration_t));

        error_code = fds_record_write(&desc, &m_default_config_record);
        APP_ERROR_CHECK(error_code);
    }
}