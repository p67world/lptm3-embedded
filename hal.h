// HAL definition
#define PORT_CTRL_SH     16
#define PORT_CTRL_FO     14
#define PORT_LEVLED1     12
#define PORT_LEVLED2     11
#define PORT_BATLED      10
#define PORT_PRELED      9
#define PORT_UART_RX     8
#define PORT_UART_TX     6
#define PORT_SEN3        13
#define PORT_SEN2        15
#define PORT_SEN1        17
#define PORT_PB          19
#define PORT_PAD         31  // AI7
#define PORT_RAW_LIGHT   30  // AI6
#define PORT_AMP         29  // AI5
#define PORT_PRE_AMP     28  // AI4
#define PORT_AMP2        26
#define PORT_AMP1        25

void hal_vInit(void);
void hal_vLedOn(uint32_t u32Port);
void hal_vLedOff(uint32_t u32Port);