void lptmFlash_init(void);
void lptmFlash_update(lptm_configuration_t const *configuration);
void lptmFlash_read(lptm_configuration_t *configuration);