#include "app_timer.h"
#include "nrf_drv_gpiote.h"
#include "lptm_pushbutton.h"

APP_TIMER_DEF(m_filteringTimerID);
APP_TIMER_DEF(m_longPressTimerID);

void    (*m_longPressAction)(void)  = NULL;
void    (*m_shortPressAction)(void) = NULL;

uint32_t    m_port = 0;

void GPIOTE_PBconfig(nrf_gpiote_polarity_t polarity);

// Press event management
void pushbutton_pressHandler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    // Disable event
    nrf_drv_gpiote_in_event_disable(m_port);

    if(action == NRF_GPIOTE_POLARITY_LOTOHI)
    {
        // Start filtering timer
        app_timer_start(m_filteringTimerID, APP_TIMER_TICKS(50), NULL);
    }
    else if(action == NRF_GPIOTE_POLARITY_HITOLO)
    {
        //NRF_LOG_INFO("SHORT PRESS!");
        app_timer_stop(m_longPressTimerID);    // Stop the timer
        if(m_shortPressAction)  m_shortPressAction();
        GPIOTE_PBconfig(NRF_GPIOTE_POLARITY_LOTOHI);
        nrf_drv_gpiote_in_event_enable(m_port, true);
    }
}

// GPIOTE config
void GPIOTE_PBconfig(nrf_gpiote_polarity_t polarity)
{
ret_code_t err_code;
nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);

    nrf_drv_gpiote_in_uninit(m_port);

    in_config.sense = polarity;
    in_config.pull = NRF_GPIO_PIN_PULLUP;

    err_code = nrf_drv_gpiote_in_init(m_port, &in_config, pushbutton_pressHandler);
    APP_ERROR_CHECK(err_code);
}

static void pushbutton_filteringTimer(void* p_context)
{
    // If pressed, it was not a glitch => manage button press
    if(nrf_gpio_pin_read(m_port) == 1)
    {
        // Wait for hi-to-low event... or timeout
        app_timer_start(m_longPressTimerID, APP_TIMER_TICKS(1500), NULL);
        GPIOTE_PBconfig(NRF_GPIOTE_POLARITY_HITOLO);
        nrf_drv_gpiote_in_event_enable(m_port, true);
    }   
    else
    {
        // Reactivate GPIO interrupt event
        nrf_drv_gpiote_in_event_enable(m_port, true);
    }
}

static void pushbutton_longPressTimer(void* p_context)
{
    // Disactivate GPIO event
    nrf_drv_gpiote_in_event_disable(m_port);

    // Still pressed => long press
    if(nrf_gpio_pin_read(m_port) == 1)
    {
        if(m_longPressAction)   m_longPressAction();
    }

    // Reactivate GPIO interrupt event
    GPIOTE_PBconfig(NRF_GPIOTE_POLARITY_LOTOHI);
    nrf_drv_gpiote_in_event_enable(m_port, true);
}



void lptm_pushbutton_init(uint32_t  port)
{
ret_code_t errorCode;   

    // Set port
    m_port = port;

    // PB press
    nrf_drv_gpiote_in_config_t pbConfig = GPIOTE_CONFIG_IN_SENSE_LOTOHI(true);
    pbConfig.pull = NRF_GPIO_PIN_PULLUP;

    errorCode = nrf_drv_gpiote_in_init(m_port, &pbConfig, pushbutton_pressHandler);
    APP_ERROR_CHECK(errorCode);
    nrf_drv_gpiote_in_event_enable(m_port, true);


    errorCode = app_timer_create(&m_filteringTimerID,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                pushbutton_filteringTimer);
    APP_ERROR_CHECK(errorCode);

    // Long press timeout
    errorCode = app_timer_create(&m_longPressTimerID,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                pushbutton_longPressTimer);
    APP_ERROR_CHECK(errorCode);
}

void lptm_pushbutton_setShortPressAction(pushbutton_action_t actionFunction)
{
    m_shortPressAction = actionFunction;
}

void lptm_pushbutton_setLongPressAction(pushbutton_action_t actionFunction)
{
    m_longPressAction = actionFunction;
}